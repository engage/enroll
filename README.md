## 平台简介
实现一个微信报名小程序，提升自己前端能力，并分享给有需要的朋友，有需要使用微信小程序做报名项目可以拿来借鉴一下。

## 演示图
<table>
    <tr>
        <td><img src="https://free.modao.cc/uploads4/images/4010/40104488/v2_pz5igc.jpg"/></td>
        <td><img src="https://free.modao.cc/uploads4/images/4010/40104553/v2_pz5ihl.png"/></td>
    </tr>
    <tr>
        <td><img src="https://free.modao.cc/uploads4/images/4010/40104553/v2_pz5ihl.png"/></td>
        <td><img src="https://free.modao.cc/uploads4/images/4010/40104565/v2_pz5ihs.jpg"/></td>
    </tr>


</table>

## QQ交流群
640049288
