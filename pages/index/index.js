const app = getApp()
var baseurl = app.globalData.baseUrl;
Page({
  data: {
    userInfo: {},
    authorize: false,
    nickName:''
  },
  onLoad: function () {
    if (app.globalData.userInfo == null || app.globalData.userInfo == '') {
      this.setData({
        authorize: true
      })
      app.userInfoReadyCallback = res => {
        this.setData({
          authorize: false,
          userInfo: res.userInfo
        })
        setTimeout(function () {
          console.log(11);
        }, 2000)
      }
    } else {
      this.setData({
        userInfo: app.globalData.userInfo
      })
    }
  },
  onGotUserInfo: function (e) {
    app.globalData.userInfo = e.detail.userInfo;
    this.setData({
      authorize: false,
      userInfo: e.detail.userInfo
    })
    
  },
  add: function (e) {
    wx.navigateTo({
      url: '../add/add?id=1'
    })
  
  },
  record:function(){
    wx.navigateTo({
      url: '../record/record?id=1'
    })
  },
  update: function () {
    wx.navigateTo({
      url: '../update/update?id=1'
    })
  },
  del: function () {
    wx.showModal({
      title: '确认删除',
      content: '删除后将无法恢复',
      success(res) {
        if (res.confirm) {
          console.log('用户点击确定')
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
    
  }
})